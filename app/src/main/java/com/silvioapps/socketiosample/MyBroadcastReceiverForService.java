package com.silvioapps.socketiosample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyBroadcastReceiverForService extends BroadcastReceiver {
    public MyBroadcastReceiverForService() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Intent newIntent = new Intent(context, MyService.class);
            newIntent.putExtra(Constants.STOP_THREAD, false);
            context.startService(newIntent);
        }

        if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            if(Utils.isOnline(null)){
                Intent newIntent = new Intent(context, MyService.class);
                newIntent.putExtra(Constants.CONNECT_KEY, true);
                context.startService(newIntent);
            }
        }
    }
}

package com.silvioapps.socketiosample;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

/**
 * Created by Silvio Guedes on 05/07/2016.
 */
public class MyNotification {
    public static final int SIMPLE_NOTIFICATION_ID = 0;
    public static final String ACTION_INTENT = "ACTION INTENT";
    private PendingIntent pendingIntent = null;
    private PendingIntent actionIntent = null;
    private Uri sound = null;

    private Bitmap bitmap = null;
    private Intent intent = null;
    private Context context = null;
    private Notification notification = null;
    private NotificationManagerCompat notificationManagerCompat = null;

    private static Handler mHandler = new Handler();
    public static final long TEN_SECONDS = 10000;

    public MyNotification(Intent intent, Context context){
        this.intent = intent;
        this.context = context;
    }

    @SuppressLint("NewApi")
    public void createNotification(Class<?> class_, String soundFile, String bigContentTitle, String summaryText, String contentTitle, String contentText, String ticker, String contentInfo, String subtext, int color, int lightsOn, int lightsOff, long[] vibratePattern){
        if(Utils.isAndroidVersionJellyBeanOrNewer()){
            TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
            taskStackBuilder.addParentStack(class_);
            taskStackBuilder.addNextIntent(intent);

            pendingIntent = taskStackBuilder.getPendingIntent(SIMPLE_NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT);
            actionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_INTENT), 0);
        }

        //String soundFile = "tone";//"sound";
        sound = Uri.parse("android.resource://"+context.getPackageName()+"/raw/"+soundFile);

        //bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.large_icon);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(bigContentTitle);
        inboxStyle.setSummaryText(summaryText);
        //inboxStyle.addLine("LINE 1");
        //inboxStyle.addLine("LINE 2");
        //inboxStyle.addLine("LINE 3");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle(contentTitle);
        builder.setContentText(contentText);
        builder.setTicker(ticker);
        builder.setWhen(System.currentTimeMillis());
        builder.setAutoCancel(true);
        builder.setOngoing(false);
        builder.setLights(color,lightsOn,lightsOff);
        builder.setSound(sound);
        builder.setVibrate(vibratePattern);

        if(Utils.isAndroidVersionJellyBeanOrNewer()) {
            builder.setContentIntent(pendingIntent);
            builder.setDeleteIntent(pendingIntent);
            //builder.addAction(R.drawable.red_icon, "ACTION INTENT", actionIntent);
        }

        builder.setContentInfo(contentInfo);
        //builder.setNumber(123);
        builder.setSubText(subtext);
        //builder.setLargeIcon(bitmap);//TODO ver tamanho do bitmap para não gerar erro de pouca memória
        builder.setStyle(inboxStyle);

        notificationManagerCompat = NotificationManagerCompat.from(context);
        notification = builder.build();
    }

    public void startNotification(){
        if(mHandler != null && context != null && notificationManagerCompat != null && notification != null) {
            try {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        notificationManagerCompat.notify(SIMPLE_NOTIFICATION_ID, notification);
                    }
                });
            } catch (Exception e) {
                Log.e("TAG", e.toString());
            }
        }
    }
}

package com.silvioapps.socketiosample;

/**
 * Created by Silvio Guedes on 13/09/2016.
 */
public class Constants {
    public static final String IP = "192.168.99.38";
    public static final int PORT = 3000;
    public static final String ADDRESS = "http://"+IP+":"+PORT;
    public static final boolean TEST = false;
    public static int TIMEOUT = 1000;
    public static final long SLEEP_TIME = 5000;//30000;
    public static final int DELAY_TIMER = 1000;
    public static final int PERIOD_TIMER = 30000;
    public static final int USERNAME = 0;
    public static final int NEW_MESSAGE = 1;
    public static final int TYPING = 2;
    public static final int STOP_TYPING = 3;
    public static final int CONNECT = 4;
    public static final int DISCONNECT = 5;
    public static final int CONNECTED = 6;
    public static final int DISCONNECTED = 7;
    public static final int LOGGED = 8;
    public static final int CONNECTION_ERROR = 9;
    public static final int TIMEOUT_ERROR = 10;
    public static final int USER_JOINED = 11;
    public static final int USER_LEFT = 12;
    public static final int STATUS = 13;
    public static final String USERNAME_KEY = "USERNAME_KEY";
    public static final String NEW_MESSAGE_KEY = "NEW_MESSAGE_KEY";
    public static final String TYPING_KEY = "TYPING_KEY";
    public static final String STOP_TYPING_KEY = "STOP_TYPING_KEY";
    public static final String CONNECT_KEY = "CONNECT_KEY";
    public static final String DISCONNECT_KEY = "DISCONNECT_KEY";
    public static final int ACTIVITY_MESSENGER_ARG_1 = 0;
    public static final int ACTIVITY_MESSENGER_ARG_2 = 0;
    public static final int TIMEOUT_TYPING = 500;
    public static final String RESULT_CODE = "RESULT_CODE";
    public static final String RESULT_RECEIVER = "RESULT_RECEIVER";
    public static final String SHARED_PREFERENCES = "SHARED_PREFERENCES";
    public static final String MESSENGER_EXTRAS = "MESSENGER_EXTRAS";
    public static final int SERVICE_MESSENGER_WHAT = 2000;
    public static final String SERVICE_MESSENGER_KEY = "SERVICE_MESSENGER_KEY";
    public static final int SERVICE_MESSENGER_ARG_1 = 0;
    public static final int SERVICE_MESSENGER_ARG_2 = 0;
    public static final String STOP_THREAD = "STOP_THREAD";
    public static final String STATUS_KEY = "STATUS_KEY";
    public static final String CONNECTED_KEY = "CONNECTED_KEY";
    public static final String DISCONNECTED_KEY = "DISCONNECTED_KEY";
    public static final String LOGGED_KEY = "LOGGED_KEY";
    public static final String CONNECTION_ERROR_KEY = "CONNECTION_ERROR_KEY";
    public static final String TIMEOUT_ERROR_KEY = "TIMEOUT_ERROR_KEY";
    public static final String USER_JOINED_KEY = "USER_JOINED_KEY";
    public static final String USER_LEFT_KEY = "USER_LEFT_KEY";
    public static final String SUCCESS_MESSAGE_RECEIVED = "SUCCESS_MESSAGE_RECEIVED";
    public static final String I_AM_ONLINE_TOO = "I_AM_ONLINE_TOO";
    public static final String I_AM_ALIVE = "I_AM_ALIVE";
}

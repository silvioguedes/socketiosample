package com.silvioapps.socketiosample;

import android.app.Activity;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.annotation.MainThread;
import android.support.v4.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

public class MyService extends Service {
    private boolean stopThread = false;
    private Thread thread = null;
    private MyNotification notification = null;
    private static Socket socket = null;
    private Handler handler = new Handler();
    private String username = "";
    private String newMessage = "";
    private String typing = "";
    private String stopTyping = "";
    private int lightsOn = 1000;
    private int lightsOff = 5000;
    private long[] vibratePattern = {100,200,300};
    private ResultReceiver resultReceiver = null;
    private boolean reconnected = false;
    private boolean forceReconnect = false;
    private long startTime = 0;
    private boolean sleep = false;
    private List<String> messagesList = null;
    private boolean joined = false;

    public MyService() {}

    @Override
    public void onCreate(){
        super.onCreate();

        stopThread = false;
        createNotification();
        instantiate();
        initMessagesList();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        if(intent != null) {
            recoverResultReceiver(intent);
            recoverBooleanValues(intent);
        }

        startThread();
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy(){
        stopThread = true;

        if(thread != null) {
            thread = null;
        }

        disconnect();
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return false;
    }

    protected void recoverResultReceiver(Intent intent){
        String resultCode_ = intent.getStringExtra(Constants.RESULT_CODE);

        if (resultCode_ != null) {
            int resultCode = Integer.parseInt(resultCode_);

            String stringMSG = intent.getStringExtra("" + resultCode);
            if (stringMSG != null) {
                onReceiveResult(resultCode, stringMSG);

                intent.removeExtra(Constants.RESULT_CODE);
                intent.removeExtra("" + resultCode);
            }
        }

        resultReceiver = intent.getParcelableExtra(Constants.RESULT_RECEIVER);
        if(resultReceiver != null){
            sendStatusToActivity();
        }

        intent.removeExtra(Constants.RESULT_RECEIVER);
    }

    protected void recoverBooleanValues(Intent intent){
        stopThread = intent.getBooleanExtra(Constants.STOP_THREAD,false);

        forceReconnect = intent.getBooleanExtra(Constants.CONNECT_KEY, false);
        if(forceReconnect){
            Log.d("TAG***","RECONNECTION");
            disconnect();
            reconnect();
        }
    }

    protected void startCounterTest(){
        if (Constants.TEST) {
            long yourmilliseconds = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("dd / MM / yyyy HH:mm:ss");
            Date resultdate = new Date(yourmilliseconds);
            String currentTime = sdf.format(resultdate);

            if (socket != null && socket.connected() && Utils.isOnline(null)) {
                Log.d("TAG***", "SOCKET CONNECTED");
                newMessage(currentTime);
            }
            else{
                Log.e("TAG***","OFFLINE");
                disconnect();
                joined = false;
            }
        }
    }

    protected void forceReconnection(){
        if (socket != null) {
            if (!socket.connected() && !getUsername().equals(null) && !getUsername().equals("") && !forceReconnect) {
                Log.e("TAG***", "DISCONNECTED, TRYING TO RECONNECT");
                disconnect();
                reconnect();
                reconnected = true;
            } else {
                if (reconnected) {
                    Log.d("TAG***", "RECONNECTED");
                    reconnected = false;
                }
            }

            sendStatusToActivity();
        }

        forceReconnect = false;
    }

    //TODO ***CAN BE DONE ON SERVER
    protected void iAmAlive(){
        if(sentIamAlive && receivedIamAlive){
            sentIamAlive = false;
            receivedIamAlive = false;
        }
        else if(sentIamAlive && !receivedIamAlive){
            sentIamAlive = false;
        }

        Log.i("TAG","receivedIamAlive "+receivedIamAlive);


        newMessage(Constants.I_AM_ALIVE);

        sentIamAlive = true;
    }

    protected void endCounterTest(){
        if (startTime == 0) {
            startTime = System.currentTimeMillis();
        }
        long endTime = System.currentTimeMillis();
        long total = endTime - startTime;
        if (total >= Constants.SLEEP_TIME) {
            startTime = 0;
            sleep = false;
        }
        else{
            sleep = true;
        }
    }

    private boolean sentIamAlive = false;
    private boolean receivedIamAlive = false;
    protected void startThread(){
        if(thread == null) {
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (!stopThread) {
                        if(!sleep) {
                            startCounterTest();
                            forceReconnection();
                            iAmAlive();
                        }

                        endCounterTest();
                    }

                    if(stopThread) {
                        //TODO
                    }
                }
            });
            thread.start();
        }
    }

    protected void createNotification(){
        Intent intent = new Intent(this, MainActivity.class);
        notification = new MyNotification(intent,getApplicationContext());
    }

    protected void initMessagesList(){
        messagesList = new ArrayList<>();
        messagesList.clear();
    }

    protected void onReceiveResult(int resultCode, String msg) {
        switch (resultCode) {
            case Constants.CONNECT:
                String message = msg;
                if (message != null && message.equals("connect")) {
                    connect();
                }
                break;
            case Constants.DISCONNECT:
                message = msg;
                if (message != null && message.equals("disconnect")) {
                    disconnect();
                }
                break;
            case Constants.USERNAME:
                username = msg;
                if (username != null && !username.equals("") && socket != null) {
                    addUser(username);
                }
                break;
            case Constants.NEW_MESSAGE:
                newMessage = msg;
                if (newMessage != null && !newMessage.equals("") && socket != null) {
                    newMessage(newMessage);
                }
                break;
            case Constants.TYPING:
                typing = msg;
                if (typing != null && !typing.equals("") && socket != null) {
                    typing();
                }
                break;
            case Constants.STOP_TYPING:
                stopTyping = msg;
                if (stopTyping != null && !stopTyping.equals("") && socket != null) {
                    stopTyping();
                }
                break;
            default:
                break;
        }
    }

    protected void sendMessageToActivity(int resultCode, String key, String msg){
        if(resultReceiver != null) {
            Bundle bundle = new Bundle();
            bundle.putString(key, msg);
            resultReceiver.send(resultCode, bundle);
        }
    }

    protected void sendMessageToActivity(int resultCode, String key, String[] msg){
        if(resultReceiver != null) {
            Bundle bundle = new Bundle();
            bundle.putStringArray(key, msg);
            resultReceiver.send(resultCode, bundle);
        }
    }

    protected void sendMessageToActivity(int resultCode, String key, boolean[] msg){
        if(resultReceiver != null) {
            Bundle bundle = new Bundle();
            bundle.putBooleanArray(key, msg);
            resultReceiver.send(resultCode, bundle);
        }
    }

    protected void sendStatusToActivity(){
        boolean connected = false;

        if(socket != null) {
            connected = socket.connected();
        }

        boolean[] msg = {connected, joined};
        sendMessageToActivity(Constants.STATUS, Constants.STATUS_KEY, msg);
    }

    protected void instantiate(){
        try {
            socket = IO.socket(Constants.ADDRESS);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    protected void connect(){
        if(socket != null && !socket.connected()) {
            socket.on(Socket.EVENT_CONNECT, onConnect);
            socket.on(Socket.EVENT_DISCONNECT, onDisconnect);
            socket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
            socket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            socket.on("new message", onNewMessage);
            socket.on("user joined", onUserJoined);
            socket.on("user left", onUserLeft);
            socket.on("typing", onTyping);
            socket.on("stop typing", onStopTyping);
            socket.on("login", onLogin);
            socket.connect();
        }
    }

    protected void reconnect(){
        if(socket != null && !socket.connected()){
            socket.io().reconnection(true);
        }
    }

    protected void reconnect(String user){
        connect();
        addUser(user);
    }

    protected void disconnect(){
        if(socket != null && socket.connected()) {
            socket.disconnect();
            socket.off(Socket.EVENT_CONNECT, onConnect);
            socket.off(Socket.EVENT_DISCONNECT, onDisconnect);
            socket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
            socket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            socket.off("new message", onNewMessage);
            socket.off("user joined", onUserJoined);
            socket.off("user left", onUserLeft);
            socket.off("typing", onTyping);
            socket.off("stop typing", onStopTyping);
            socket.off("login", onLogin);
        }

        joined = false;
    }

    protected void addUser(String user){
        socket.emit("add user", user);
    }

    protected void newMessage(String message){
        socket.emit("new message", message);
    }

    protected void typing(){
        socket.emit("typing", typing);
    }

    protected void stopTyping(){
        socket.emit("stop typing", stopTyping);
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if(handler != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        sendStatusToActivity();
                        sendMessageToActivity(Constants.CONNECTED, Constants.CONNECTED_KEY,"connected");
                    }
                });
            }
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if(handler != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        disconnect();
                        sendStatusToActivity();
                        sendMessageToActivity(Constants.DISCONNECTED,Constants.DISCONNECTED_KEY,"disconnected");
                    }
                });
            }
        }
    };

    private Emitter.Listener onLogin = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if(handler != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            int numUsers = data.getInt("numUsers");
                        } catch (JSONException e) {
                            return;
                        }

                        joined = true;
                        sendStatusToActivity();
                        sendMessageToActivity(Constants.LOGGED,Constants.LOGGED_KEY,"logged");
                    }
                });
            }
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if(handler != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        sendMessageToActivity(Constants.CONNECTION_ERROR,Constants.CONNECTION_ERROR_KEY,"error");
                    }
                });
            }
        }
    };

    protected void sendAndReceiveStatus(final String username, final String message, String[] array){
        //todo I SENT THE MSG, SO I RECEIVE THE 'OK' FROM THE RECEIVER
        if(message.contains(Constants.SUCCESS_MESSAGE_RECEIVED)) {
            if (handler != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(getApplicationContext(),"MESSAGE SENT SUCCESSFULLY!!!",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }


        //TODO ***CAN BE DONE ON SERVER
        //todo VERIFY IF THE USER IS ALIVE YET
        else if(message.contains(Constants.I_AM_ALIVE)) {
            receivedIamAlive = true;
        }


        //todo ADD THE USER TO THE ONLINE USERS LIST
        else if(message.contains(Constants.I_AM_ONLINE_TOO)) {
            sendMessageToActivity(Constants.USER_JOINED, Constants.USER_JOINED_KEY, array);
        }
        //todo I RECEIVED THE MSG, SO I SEND 'OK' TO SENDER
        else{
            newMessage(Constants.SUCCESS_MESSAGE_RECEIVED);

            if(MyService.this.username != null && !MyService.this.username.equals(username) && notification != null){
                if(handler != null && notification != null){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            notification.createNotification(MainActivity.class, "tone",message,"Mensagem de "+username,"Notificação",message,"Nova Mensagem","Informação","Subtexto",
                                    Color.RED,lightsOn, lightsOff,vibratePattern);
                        }
                    });

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            notification.startNotification();
                        }
                    });
                }
            }
        }
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];
            try {
                final String username = data.getString("username");
                final String message = data.getString("message");
                String[] array = {username, message};

                sendMessageToActivity(Constants.NEW_MESSAGE, Constants.NEW_MESSAGE_KEY, array);
                saveMessages(username + ": " + message + "\n");
                sendAndReceiveStatus(username, message, array);

            } catch (JSONException e) {
                return;
            }
        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if(handler != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            String username = data.getString("username");
                            int numUsers = data.getInt("numUsers");

                            if(!username.equals(getUsername())) {
                                String[] array = {username, "" + numUsers};
                                sendMessageToActivity(Constants.USER_JOINED, Constants.USER_JOINED_KEY, array);
                            }

                            //TODO SEND A MESSAGE "I AM ONLINE TOO"
                            newMessage(Constants.I_AM_ONLINE_TOO);
                        } catch (JSONException e) {
                            return;
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if(handler != null){
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            String username = data.getString("username");
                            int numUsers = data.getInt("numUsers");

                            String[] array = {username, ""+numUsers};
                            sendMessageToActivity(Constants.USER_LEFT,Constants.USER_LEFT_KEY,array);
                        } catch (JSONException e) {
                            return;
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (handler != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            String username = data.getString("username");
                            sendMessageToActivity(Constants.TYPING,Constants.TYPING_KEY,username);
                        } catch (JSONException e) {
                            return;
                        }
                    }
                });
            }
        }
    };

    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (handler != null) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            String username = data.getString("username");
                            sendMessageToActivity(Constants.STOP_TYPING,Constants.STOP_TYPING_KEY,username);
                        } catch (JSONException e) {
                            return;
                        }
                    }
                });
            }
        }
    };

    protected String getUsername(){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String username = sharedPref.getString(getString(R.string.username), "");

        return username;
    }

    protected void saveMessages(String messages){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.messages), getMessages() + messages);

        editor.commit();
    }

    protected String getMessages(){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String messages = sharedPref.getString(getString(R.string.messages), "");

        return messages;
    }
}

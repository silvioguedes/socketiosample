package com.silvioapps.socketiosample;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by Silvio Guedes on 09/09/2016.
 */
public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{
    private List<String> valuesList = null;
    private Context context = null;
    private ViewHolder viewHolder = null;

    public RecyclerViewAdapter(Context context,List<String> stringList){
        this.valuesList = stringList;
        this.context = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textView = null;

        public ViewHolder(View v){
            super(v);

            textView = (TextView)v.findViewById(R.id.textView);
        }
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_layout,parent,false);
        viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        holder.textView.setText(valuesList.get(position));
    }

    @Override
    public int getItemCount(){
        return valuesList == null ? 0 : valuesList.size();
    }
}

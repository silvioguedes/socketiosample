package com.silvioapps.socketiosample;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.List;

/**
 * Created by Silvio Guedes on 15/08/2016.
 */
public class Utils {

    public static boolean isOnline(String website)
    {
        String website_ = website;

        if (website == null)
        {
            website_ = "www.google.com";
        }

        try
        {
            Process p1 = Runtime.getRuntime().exec("ping -c 1 "+website_);
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal == 0);
            return reachable;
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public static boolean checkServer(String ipAddress){
        try
        {
            boolean status = InetAddress.getByAddress(ipAddress.getBytes()).isReachable(5000); //Timeout = 5000 milli seconds

            return status;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }


    public static boolean isServerReachable(final String addr, final int openPort, final int timeOutMillis) {
        // Any Open port on other machine
        // openPort =  22 - ssh, 80 or 443 - webserver, 25 - mailserver etc.
        class MyAsyncTask extends AsyncTask<Object, Object, Boolean> {
            protected void onPreExecute() {}

            protected Boolean doInBackground(Object... urls) {
                try {
                    Socket soc = new Socket();
                    soc.connect(new InetSocketAddress(addr, openPort), timeOutMillis);
                    return true;
                } catch (IOException ex) {
                    return false;
                }
            }

            protected void onPostExecute(Boolean result) {}
        }

        boolean result = false;
        try {
            result = new MyAsyncTask().execute().get();
        }
        catch (Exception e){
            e.printStackTrace();
        }

        return result;
    }

    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isApplicationBroughtToBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }

        return false;
    }

    public static boolean isAppRunningOnBackground(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        //If your app is the process in foreground, then it's not in running in background
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public static boolean isAndroidVersionJellyBeanOrNewer(){
        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.JELLY_BEAN) {
            return true;
        }

        return false;
    }

    public static void appendTextView(TextView textView, String text){
        if(textView != null) {
            textView.append(text);
            textView.postDelayed(null, 0);
        }
    }

    public static  void scrollDown(final ScrollView scrollView){
        if(scrollView != null) {
            scrollView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                }
            }, 0);
        }
    }
}

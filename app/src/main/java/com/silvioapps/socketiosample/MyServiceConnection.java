package com.silvioapps.socketiosample;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;

/**
 * Created by Silvio Guedes on 02/07/2016.
 */
public class MyServiceConnection implements ServiceConnection {
    private boolean isBound = false;

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service){
        isBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName){
        isBound = false;
    }

    public boolean isBound(){
        return isBound;
    }

    public void setIsBound(boolean isBound){
        this.isBound = isBound;
    }
}

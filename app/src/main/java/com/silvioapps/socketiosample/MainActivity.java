package com.silvioapps.socketiosample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements MyResultReceiver.Receiver{
    private static TextView messageReceivedTextView = null;
    private static ScrollView scrollView = null;

    private TextInputLayout textInputLayout = null;
    private EditText setUsernameEditText = null;
    private EditText sendMessageEditText = null;
    private Button joinButton = null;
    private Button sendButton = null;
    private Button connectButton = null;
    private String username = null;
    private Thread mainThread = null;
    private boolean isTyping = false;
    private boolean out = false;
    private long lastTime = -1;
    private MyServiceConnection serviceConnection = null;
    private Intent serviceIntent = null;
    public boolean connected = false;
    private boolean joined = false;
    private String messages = "";
    private Handler handler = null;

    private MyResultReceiver myResultReceiver = null;
    public static boolean isVisible = false;

    private static RecyclerView recyclerView = null;
    private static RecyclerViewAdapter recyclerViewAdapter = null;
    private static List<String> onlineUsersList = null;
    private static RecyclerView.LayoutManager recyclerViewLayoutManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        verifyServerStatus();

        initOnlineUsersList();

        recoverStatus(false);

        createControls();

        updateControls();

        startThread();

        startService();
    }

    @Override
    protected void onResume() {
        recoverStatus(true);
        updateControls();

        messageReceivedTextView = (TextView)findViewById(R.id.messageReceivedTextView);
        if(messageReceivedTextView != null) {
            messageReceivedTextView.setText(messages);
        }

        isVisible = true;

        super.onResume();
    }

    @Override
    protected void onPause() {
        if (serviceConnection != null && serviceConnection.isBound()) {
            serviceConnection.setIsBound(false);
        }

        saveStatus(username, connected, joined, new HashSet<String>(onlineUsersList));

        isVisible = false;

        super.onPause();
    }

    @Override
    protected void onStop(){
        saveStatus(username, connected, joined, new HashSet<String>(onlineUsersList));

        isVisible = false;

        super.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode){
            case Constants.CONNECTED:
                onConnected();
                break;
            case Constants.DISCONNECTED:
                onDisconnected();
                break;
            case Constants.LOGGED:
                onLogged();
                break;
            case Constants.CONNECTION_ERROR:
                onConnectionError(resultData);
                break;
            case Constants.NEW_MESSAGE:
                onNewMessage(resultData);
                break;
            case Constants.USER_JOINED:
                onUserJoined(resultData);
                break;
            case Constants.USER_LEFT:
                onUserLeft(resultData);
                break;
            case Constants.TYPING:
                onTyping(resultData);
                break;
            case Constants.STOP_TYPING:
                onStopTyping(resultData);
                break;
            case Constants.STATUS:
                onStatus(resultData);
                break;
            default:
                break;
        }
    }

    protected void verifyServerStatus(){
        if(!Utils.isServerReachable(Constants.IP,Constants.PORT,Constants.TIMEOUT)){
            Toast.makeText(this,getString(R.string.server_error),Toast.LENGTH_LONG).show();
        }
    }

    protected void initOnlineUsersList(){
        onlineUsersList = new ArrayList<>();
        onlineUsersList.clear();
    }

    protected void startService(){
        myResultReceiver = new MyResultReceiver(new Handler());
        myResultReceiver.setReceiver(MainActivity.this);

        if(!Utils.isServiceRunning(this, MyService.class)) {
            serviceConnection = new MyServiceConnection();

            serviceIntent = new Intent(MainActivity.this,MyService.class);
            serviceIntent.putExtra(Constants.STOP_THREAD,false);

            startService(serviceIntent);
        }
    }

    protected void startThread(){
        handler = new Handler();
        mainThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!out){
                    long diff = System.currentTimeMillis() - lastTime;
                    if(diff > Constants.TIMEOUT_TYPING){
                        if(handler != null){
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if(isTyping) {
                                        sendMessageToService(Constants.STOP_TYPING, "stop typing");
                                    }

                                    lastTime = System.currentTimeMillis();
                                    isTyping = false;
                                }
                            });
                        }
                    }
                }
            }
        });
        mainThread.start();
    }

    protected void createControls(){
        setUsernameEditText = (EditText)findViewById(R.id.usernameEditText);
        setUsernameEditText.setText(username);

        sendMessageEditText = (EditText)findViewById(R.id.sendMessageEditText);
        sendMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //TODO
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!isTyping){
                    sendMessageToService(Constants.TYPING, "typing");

                    isTyping = true;
                }

                lastTime = System.currentTimeMillis();
            }

            @Override
            public void afterTextChanged(Editable s) {
                //TODO
            }
        });

        textInputLayout = (TextInputLayout)findViewById(R.id.text_input_layout);

        connectButton = (Button)findViewById(R.id.connectButton);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(connectButton.getText().equals(getString(R.string.connect))){
                    sendMessageToService(Constants.CONNECT, "connect");

                    connectButton.setText(getString(R.string.disconnect));

                    if(joinButton != null){
                        joinButton.setText(getString(R.string.join));
                        joinButton.setEnabled(true);
                    }

                    connected = true;
                }
                else{
                    sendMessageToService(Constants.DISCONNECT, "disconnect");

                    connectButton.setText(getString(R.string.connect));

                    if(joinButton != null){
                        joinButton.setText(getString(R.string.join));
                        joinButton.setEnabled(false);
                    }

                    if(setUsernameEditText != null){
                        setUsernameEditText.setEnabled(true);
                    }
                    if(sendButton != null){
                        sendButton.setEnabled(false);
                    }

                    connected = false;

                    onDisconnected();
                }
            }
        });

        joinButton = (Button)findViewById(R.id.joinButton);
        joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(setUsernameEditText != null) {
                    username = setUsernameEditText.getText().toString();

                    if(username != null) {
                        if(connected && joinButton.getText().equals(getString(R.string.join))){
                            sendMessageToService(Constants.USERNAME, username);

                            saveStatus(username, connected, joined, new HashSet<String>(onlineUsersList));

                            joinButton.setText(getString(R.string.joined));
                            joinButton.setEnabled(false);

                            if(setUsernameEditText != null){
                                setUsernameEditText.setEnabled(false);
                            }

                            if(sendButton != null){
                                sendButton.setEnabled(true);
                            }
                        }
                        else{
                            if(setUsernameEditText != null){
                                setUsernameEditText.setEnabled(true);
                            }

                            if(sendButton != null){
                                sendButton.setEnabled(false);
                            }
                        }
                    }
                }
            }
        });

        sendButton = (Button)findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sendMessageEditText != null) {
                    sendMessageToService(Constants.NEW_MESSAGE, sendMessageEditText.getText().toString());
                }
            }
        });

        messageReceivedTextView = (TextView)findViewById(R.id.messageReceivedTextView);
        if(messageReceivedTextView != null) {
            messageReceivedTextView.setText(messages);
        }

        scrollView = (ScrollView)findViewById(R.id.scrollView);
        Utils.scrollDown(scrollView);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        recyclerViewLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewLayoutManager);
        recyclerViewAdapter = new RecyclerViewAdapter(this, onlineUsersList);
        recyclerView.setAdapter(recyclerViewAdapter);
    }

    protected void sendMessageToService(int resultCode, String msg) {
        Intent intent = new Intent(MainActivity.this, MyService.class);
        intent.putExtra(Constants.RESULT_CODE,""+resultCode);
        intent.putExtra(""+resultCode,msg);
        intent.putExtra(Constants.RESULT_RECEIVER,myResultReceiver);
        startService(intent);
    }

    protected void sendMessageToService(String key, boolean msg) {
        Intent intent = new Intent(MainActivity.this, MyService.class);
        intent.putExtra(key,msg);
        startService(intent);
    }

    protected void saveStatus(String username, boolean connected, boolean joined, Set<String> onlineUsersSet){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.username), username);
        editor.putBoolean(getString(R.string.connect_status), connected);
        editor.putBoolean(getString(R.string.join_status), joined);
        editor.putStringSet(getString(R.string.online_users_set),onlineUsersSet);

        editor.commit();
    }

    protected void saveMessages(String messages){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.messages), getMessages() + messages);

        editor.commit();
    }

    protected void saveOnlineUsers(Set<String> onlineUsersSet){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putStringSet(getString(R.string.online_users_set),onlineUsersSet);

        editor.commit();
    }

    protected String getMessages(){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        String messages = sharedPref.getString(getString(R.string.messages), "");

        return messages;
    }

    protected boolean getConnectStatus(){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        boolean connected = sharedPref.getBoolean(getString(R.string.connect_status), false);

        return connected;
    }

    protected boolean getJoinStatus(){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        boolean joined = sharedPref.getBoolean(getString(R.string.join_status), false);

        return joined;
    }

    protected String getUsername(){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        String username = sharedPref.getString(getString(R.string.username), "");

        return username;
    }

    protected Set<String> getOnlineUsers(){
        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        Set<String> set = sharedPref.getStringSet(getString(R.string.online_users_set), null);

        return set;
    }

    //I AM CONNECTED TO THE GROUP
    protected void onConnected(){
        connected = true;
    }

    //I LEFT THE GROUP
    protected void onDisconnected(){
        connected = false;
        joined = false;

        if(isVisible) {
            if(recyclerView != null && onlineUsersList != null){
                for(int i=0;i<onlineUsersList.size();i++){
                    RecyclerViewAdapter.ViewHolder viewHolder = (RecyclerViewAdapter.ViewHolder)recyclerView.findViewHolderForAdapterPosition(i);
                    if(viewHolder != null && viewHolder.textView != null) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorOffline));
                    }
                }
            }
        }
    }

    protected void onLogged(){
        joined = true;
    }

    protected void onConnectionError(Bundle resultData){
        String message = (String)resultData.get(Constants.CONNECTION_ERROR_KEY);
    }

    protected void onNewMessage(Bundle resultData){
        String[] array = resultData.getStringArray(Constants.NEW_MESSAGE_KEY);
        String username = array[0];
        String message = array[1];

        if(isVisible){
            Utils.appendTextView(messageReceivedTextView, username + ": " + message + "\n");
            Utils.scrollDown(scrollView);
        }
    }

    //NEW USER JOINED TO THE GROUP
    protected void onUserJoined(Bundle resultData){
        String[] array = resultData.getStringArray(Constants.USER_JOINED_KEY);
        String username = array[0];
        String numUsers = array[1];

        if(!isUsernameInUse(username)) {
            if (isVisible) {
                if (recyclerView != null && onlineUsersList != null) {
                    RecyclerViewAdapter.ViewHolder viewHolder = (RecyclerViewAdapter.ViewHolder) recyclerView.findViewHolderForAdapterPosition(onlineUsersList.indexOf(username));
                    if (viewHolder != null && viewHolder.textView != null) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorOnline));
                    } else {
                        if (recyclerViewAdapter != null) {
                            onlineUsersList.add(username);
                            recyclerViewAdapter.notifyDataSetChanged();
                        }
                    }
                }
            }
        }
    }

    //USER LEFT THE GROUP
    protected void onUserLeft(Bundle resultData){
        String[] array = resultData.getStringArray(Constants.USER_LEFT_KEY);
        String username = array[0];
        String numUsers = array[1];

        joined = false;

        if(isVisible) {
            if(recyclerView != null && onlineUsersList != null){
                RecyclerViewAdapter.ViewHolder viewHolder = (RecyclerViewAdapter.ViewHolder)recyclerView.findViewHolderForAdapterPosition(onlineUsersList.indexOf(username));
                if(viewHolder != null && viewHolder.textView != null) {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.colorOffline));
                }
            }
        }
    }

    protected void onTyping(Bundle resultData){
        String username = (String)resultData.get(Constants.TYPING_KEY);

        if(isVisible) {
            if(recyclerView != null && onlineUsersList != null){
                RecyclerViewAdapter.ViewHolder viewHolder = (RecyclerViewAdapter.ViewHolder)recyclerView.findViewHolderForAdapterPosition(onlineUsersList.indexOf(username));
                if(viewHolder != null && viewHolder.textView != null) {
                    viewHolder.textView.append(getString(R.string.is_typing));
                }
            }
        }
    }

    protected void onStopTyping(Bundle resultData){
        String username = (String)resultData.get(Constants.STOP_TYPING_KEY);

        if(isVisible) {
            if(recyclerView != null && onlineUsersList != null){
                RecyclerViewAdapter.ViewHolder viewHolder = (RecyclerViewAdapter.ViewHolder)recyclerView.findViewHolderForAdapterPosition(onlineUsersList.indexOf(username));
                if(viewHolder != null && viewHolder.textView != null) {
                    viewHolder.textView.setText(username);
                }
            }
        }
    }

    protected void onStatus(Bundle resultData){
        recoverStatus(false);

        boolean[] array = resultData.getBooleanArray(Constants.STATUS_KEY);
        connected = array[0];
        joined = array[1];

        updateControls();
    }

    protected void recoverStatus(boolean onResume){
        username = getUsername();
        messages = getMessages();
        connected = getConnectStatus();
        joined = getJoinStatus();

        //TODO ***CAN BE DONE ON SERVER
        if(onResume && getOnlineUsers() != null && onlineUsersList != null) {
            onlineUsersList.clear();
            onlineUsersList.addAll(getOnlineUsers());
        }
    }

    protected void updateControls(){
        if(setUsernameEditText != null){
            if(connected){
                if(joined){
                    setUsernameEditText.setEnabled(false);
                }
                else{
                    setUsernameEditText.setEnabled(true);
                }
            }
            else{
                setUsernameEditText.setEnabled(true);
            }
        }

        if(connectButton != null){
            if(connected){
                connectButton.setText(getString(R.string.disconnect));
            }
            else{
                connectButton.setText(getString(R.string.connect));
            }
        }

        if(joinButton != null){
            if(connected){
                if(joined){
                    joinButton.setText(getString(R.string.joined));
                    joinButton.setEnabled(false);
                }
                else{
                    joinButton.setText(getString(R.string.join));
                    joinButton.setEnabled(true);
                }
            }
            else{
                joinButton.setText(getString(R.string.join));
                joinButton.setEnabled(false);
            }
        }

        if(sendButton != null){
            if(joined){
                sendButton.setEnabled(true);
            }
            else{
                sendButton.setEnabled(false);
            }
        }
    }

    protected boolean isUsernameInUse(String username){
        if(textInputLayout != null) {
            if (setUsernameEditText.getText().toString().equals(username)) {
                textInputLayout.setErrorEnabled(true);
                textInputLayout.setError(getString(R.string.username_error));

                sendMessageToService(Constants.DISCONNECT, "disconnect");

                return true;
            } else {
                textInputLayout.setErrorEnabled(false);
            }
        }

        return false;
    }
}

